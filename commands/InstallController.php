<?php

namespace app\commands;

use splynx\base\BaseInstallController;

/**
 * Class InstallController
 * @package app\commands
 */
class InstallController extends BaseInstallController
{
    /**
     * @inheritdoc
     */
    public $module_status = self::MODULE_STATUS_ENABLED;

    /**
     * @inheritdoc
     */
    public static $minimumSplynxVersion = '3.1';

    /**
     * @inheritdoc
     * @return string
     */
    public function getAddOnTitle()
    {
        // Db column varchar(64)
        return 'Splynx Add-on Learning';
    }

    /**
     * @inheritdoc
     */
    public function getModuleName()
    {
        // Db column varchar(32)
        return 'splynx_addon_learning';
    }

    /**
     * @inheritdoc
     */
    public function getApiPermissions()
    {
        return [
            [
                'controller' => 'api\admin\administration\Administrators',
                'actions' => ['index'],
            ],
            [
                'controller' => 'api\admin\customers\Customer',
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function getEntryPoints()
    {
        return [
            [
                'name' => 'learning_main',
                'title' => $this->getAddOnTitle(),
                'root' => 'controllers\admin\CustomersController',
                'place' => 'admin',
                'url' => '%2Flearning%2F',
                'icon' => 'fa-puzzle-piece',
            ],
        ];
    }
}
